#!/bin/bash

echo "Building and up environment"

docker-compose up -d
sleep 15

echo "Running composer install"

docker exec urlshortener_php_1 /bin/bash -c "su docker; cd /var/www/urlshortener;  composer install"
sleep 15

echo "Creating database and schema"

docker exec urlshortener_php_1 /bin/bash -c "su docker; cd /var/www/urlshortener; app/console doctrine:database:create; app/console doctrine:schema:update --force"
sleep 15

docker exec urlshortener_php_1 /bin/bash -c "chown -R docker:docker /var/www"

echo "Done, see the API documentation in http://urlshortener.dev:8080/api/doc"