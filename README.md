Url Shortener API
=================

Rest generator of short URLs.

Environment Setup
-----------------

The entire environment is based in [Docker](https://www.docker.com) and you need install the [Docker tools](https://www.docker.com).

### Requirements

- Install [Docker](http://docs.docker.com/linux/started/)
- Install [Docker Compose](https://docs.docker.com/compose/install/) (We recommend you to install using python pip)

### Install script
Check if ports 80, 443 and 3306 are available.

After clone the repository, run install.sh script

```sh
$ ./install.sh
```

### Testing your environment

Check system connection

```sh
$ curl -H "Content-Type:application/json" http://urlshortener.dev:8080/api/v1/ping
# {"message":"pong"}
```

### Get full endpoint documentation
http://urlshortener.dev:8080/api/doc

