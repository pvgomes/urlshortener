<?php

namespace AppBundle\Application\Api\v1\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JsonSchema\Uri\UriRetriever;
use AppBundle\Application\Api\ApiController;

class SystemController extends ApiController
{
    /**
     * @var \AppBundle\Application\Service
     */
    private $applicationService;

    /**
     * @return \AppBundle\Application\Service
     */
    public function getApplicationService()
    {
        if (!($this->applicationService instanceof \AppBundle\Application\Service)) {
            $this->applicationService = $this->get('application_service');
        }
        return $this->applicationService;
    }

    /**
     *
     * @Rest\Get("/api/v1/ping")
     *
     * @ApiDoc(
     *  section="System",
     *  description="Tests the service availability",
     *  statusCodes={
     *      200="Available service",
     *      400="Unauthorized service",
     *      500="Unavailable service"
     *  },
     *  tags={
     *      "stable" = "#6BB06C"
     *  },
     * views = { "default", "seller", "market"}
     * )
     */
    public function ping()
    {
        $response = new JsonResponse(['message' => 'pong']);
        return $response;
    }

    /**
     * <strong>Request body</strong>:<br>
     * <pre>{
     *         "id": "chaordic"
     * }</pre>
     * <strong>Response body</strong>:<br>
     * <pre>{
     *    "id": chaordic
     * }</pre>
     *
     * @Rest\Post("/api/v1/users")
     *
     * @ApiDoc(
     *  section="User",
     *  description="Users creation",
     *  statusCodes={
     *      201="User created",
     *      400="Request error",
     *      500="Server error, try again later"
     *  },
     *  tags={
     *      "stable" = "#6BB06C"
     *  }
     * )
     */
    public function addUser()
    {
        $request = $this->getRequest();
        $userId = $request->get('id');

        try {
            $user = $this->getApplicationService()->createUser($userId);
            $response = new JsonResponse(['id' => $user->getId()], 201);
        } catch (\DomainException $exception) {
            $response = new JsonResponse(['message' => $exception->getMessage()], 400);
        } catch (UniqueConstraintViolationException $exception) {
            $response = new JsonResponse(['message' => "{$userId} already exists"],409);
        } catch (\Exception $exception) {
            $response = new JsonResponse(['message' => "Internal server error, try again later"], 500);
        }

        return $response;
    }


    /**
     * @Rest\Delete("/api/v1/users/{userId}")
     *
     * @ApiDoc(
     *  section="User",
     *  description="User deletion",
     *  statusCodes={
     *      204="User was deleted",
     *      400="Request error",
     *      500="Server error, try again later"
     *  },
     *  tags={
     *      "stable" = "#6BB06C"
     *  }
     * )
     */
    public function deleteUser($userId)
    {
        try {
            $this->getApplicationService()->deleteUser($userId);
            $response = new JsonResponse(null, 204);
        } catch (\DomainException $exception) {
            $response = new JsonResponse(['message' => $exception->getMessage()], 400);
        } catch (UniqueConstraintViolationException $exception) {
            $response = new JsonResponse(['message' => "{$userId} already exists"],409);
        } catch (\Exception $exception) {
            $response = new JsonResponse(['message' => "Internal server error, try again later"], 500);
        }

        return $response;
    }


    /**
     * <strong>Request body</strong>:<br>
     * <pre>{
     *         "url": "http://www.chaordic.com.br/folks"
     * }</pre>
     * <strong>Response body</strong>:<br>
     * <pre>
     * {
     *      "id": "23094",
     *      "hits": 0,
     *      "url": "http://www.chaordic.com.br/folks",
     *      "shortUrl": "http://<host>[:<port>]/asdfeiba"
     * }
     * </pre>
     *
     * @Rest\Post("/api/v1/users/{userId}/urls")
     *
     * @ApiDoc(
     *  section="Url",
     *  description="Create a short URL based on full url",
     *  statusCodes={
     *      201="Url was created",
     *      400="Request error",
     *      500="Server error, try again later"
     *  },
     *  tags={
     *      "stable" = "#6BB06C"
     *  }
     * )
     */
    public function addUrl($userId)
    {
        $request = $this->getRequest();
        $url = $request->get('url');

        try {
            $response = $this->getApplicationService()->createUrl($userId, $url);
            $response = new JsonResponse($response, 201);
        } catch (\DomainException $exception) {
            $response = new JsonResponse(['message' => $exception->getMessage()], 400);
        } catch (UniqueConstraintViolationException $exception) {
            $response = new JsonResponse(['message' => "{$userId} already exists"],409);
        } catch (\Exception $exception) {
            $response = new JsonResponse(['message' => "Internal server error, try again later"], 500);
        }

        return $response;
    }

    /**
     *
     * @Rest\Get("/api/v1/stats/{id}")
     *
     * @ApiDoc(
     *  section="Url",
     *  description="Show url stats",
     *  statusCodes={
     *      200="url stats resources stats",
     *      400="Request error",
     *      500="Server error, try again later"
     *  },
     *  tags={
     *      "stable" = "#6BB06C"
     *  }
     * )
     */
    public function statsId($id)
    {

        try {
            $url = $this->getApplicationService()->statsUrl($id);
            $response = new JsonResponse($url, 200);
        } catch (\DomainException $exception) {
            $response = new JsonResponse(['message' => $exception->getMessage()], 404);
        } catch (\Exception $exception) {
            $response = new JsonResponse(['message' => "Internal server error, try again later"], 500);
        }
        
        return $response;
    }
    
        /**
     *
     * @Rest\Get("/api/v1/stats")
     *
     * @ApiDoc(
     *  section="Url",
     *  description="Show all url stats",
     *  statusCodes={
     *      200="url stats resources stats",
     *      400="Request error",
     *      500="Server error, try again later"
     *  },
     *  tags={
     *      "stable" = "#6BB06C"
     *  }
     * )
     */
    public function stats()
    {

        try {
            $url = $this->getApplicationService()->stats();
            $response = new JsonResponse($url, 200);
        } catch (\DomainException $exception) {
            $response = new JsonResponse(['message' => $exception->getMessage()], 404);
        } catch (\Exception $exception) {
            $response = new JsonResponse(['message' => "Internal server error, try again later"], 500);
        }
        
        return $response;
    }
}
