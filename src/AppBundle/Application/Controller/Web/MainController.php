<?php

namespace AppBundle\Application\Controller\Web;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller
{
    /**
     * @Route("/", name="dashboard")
     */
    public function indexAction()
    {
        return $this->render('web/dashboard.html.twig');
    }

    /**
     * @Route("/login", name="login")
     */
    public function loginAction()
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('web/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }
    
    
    /**
     * @Route("/short/{urlId}", name="short")
     */
    public function shortAction($urlId)
    {
        $applicationService = $this->get('application_service');
        $url = $applicationService->getByShort($urlId);
        $url->addHit();
        $applicationService->saveUrl($url);
        
        return $this->redirect($url->getRealUrl(), 301);
    }
    

    /**
     * @Route("/login_check", name="login_check")
     */
    public function loginCheckAction()
    {
        // this action will not be executed,
        // as the route is handled by the Security system
    }
}
