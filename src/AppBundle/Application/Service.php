<?php

namespace AppBundle\Application;

use AppBundle\Domain\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

use AppBundle\Domain\User;

class Service {

    /**
     * @var ContainerInterface
     */
    protected $serviceContainer;

    public function __construct(ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
    }

    /**
     * @param $userId
     * @return User
     */
    public function createUser($userId)
    {
        $userRepository = $this->serviceContainer->get('user_repository');
        $user = new User();
        $user->setId($userId);
        $userRepository->add($user);
        return $user;
    }

    public function deleteUser($userId)
    {
        $userRepository = $this->serviceContainer->get('user_repository');
        $user = $userRepository->get($userId);
        $userRepository->remove($user);
        return true;
    }

    public function createUrl($userId, $url)
    {
        $urlRepository = $this->serviceContainer->get('url_repository');
        $userRepository = $this->serviceContainer->get('user_repository');
        $user = $userRepository->get($userId);

        $urlEntity = $urlRepository->getByRealUrl($url);

        if (!($urlEntity instanceof Url)) {
            $urlEntity = new Url();
            $urlEntity->setRealUrl($url);
            $urlEntity->setUser($user);
            $urlRepository->add($urlEntity);
        }

         $short = $urlEntity->getShortUrl();
         $urlEntity->setShortUrl($this->serviceContainer->getParameter("base_url"). $short);
        
        return $urlEntity->toArray();
    }
    
    public function statsUrl($id)
    {
         $urlRepository = $this->serviceContainer->get('url_repository');
         $urlEntity = $urlRepository->get($id);
         
         if (!($urlEntity instanceof Url)) {
             throw new \DomainException("url not exists");
         }
         
         
         return $urlEntity->toArray();
         
    }
    
    public function stats()
    {
         $arrayResponse = [];
         $urlRepository = $this->serviceContainer->get('url_repository');
         $urls = $urlRepository->getByHits();
         
         $arrayResponse['hits'] = 0;
         $arrayResponse['urlCount'] = count($urls);
         
         $arrayResponse['topUrls'] = [];
         
         $count = 1;
         foreach ($urls as $url) {
            $arrayResponse['topUrls'][] = $url->toArray($this->serviceContainer->getParameter("base_url"));    
            $arrayResponse['hits'] += $url->getHits();
            $count++;
            if ($count >= 10) {
                continue;
            }
         }
         
         
         return $arrayResponse;
         
    }
        
    public function getByShort($short)
    {
         $urlRepository = $this->serviceContainer->get('url_repository');
         $urlEntity = $urlRepository->getByShortUrl($short);
         
         if (!($urlEntity instanceof Url)) {
             throw new \DomainException("url not exists");
         }
         
         return $urlEntity;         
    }
    
        
    public function saveUrl(Url $url)
    {
         $urlRepository = $this->serviceContainer->get('url_repository');
         $urlRepository->add($url);       
    }
   

}