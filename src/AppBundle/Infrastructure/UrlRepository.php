<?php

namespace AppBundle\Infrastructure;

use AppBundle\Domain;
use AppBundle\Domain\Url;

class UrlRepository extends EntityRepository implements Domain\UrlRepository
{
    private $entityPath = 'AppBundle\Domain\Url';

    /**
     * {@inheritdoc}
     */
    public function get($id)
    {
        $repository = $this->getRepository();
        return $repository->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function getAll()
    {
        $repository = $this->getRepository()
            ->createQueryBuilder('p')
            ->orderBy('p.createdAt', 'DESC')
            ->getQuery();

        return $repository->getResult();
    }
    
    public function getByHits()
    {
        $repository = $this->getRepository()
            ->createQueryBuilder('p')
            ->orderBy('p.hits', 'DESC')
            ->getQuery();

        return $repository->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function add(Url $url){
        $this->getEntityManager()->persist($url);
        $this->getEntityManager()->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getByRealUrl($realUrl)
    {
        $url = $this->getRepository()
            ->findOneBy(['realUrl' => $realUrl]);

        return $url;
    }
    
        /**
     * {@inheritdoc}
     */
    public function getByShortUrl($shortUrl)
    {
        $url = $this->getRepository()
            ->findOneBy(['shortUrl' => $shortUrl]);

        return $url;
    }


    /**
     * {@inheritdoc}
     */
    public function remove(Url $url)
    {
        $this->getEntityManager()->remove($url);
        $this->getEntityManager()->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityPath()
    {
        return $this->entityPath;
    }
}
