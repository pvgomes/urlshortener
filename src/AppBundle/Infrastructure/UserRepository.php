<?php

namespace AppBundle\Infrastructure;

use AppBundle\Domain;
use AppBundle\Domain\User;

class UserRepository extends EntityRepository implements Domain\UserRepository
{
    private $entityPath = 'AppBundle\Domain\User';

    public function get($id)
    {
        $repository = $this->getRepository();
        return $repository->find($id);
    }


    public function getAll()
    {
        $repository = $this->getRepository()
            ->createQueryBuilder('p')
            ->orderBy('p.createdAt', 'DESC')
            ->getQuery();

        return $repository->getResult();
    }

    public function getByUserName($userName)
    {
        $repository = $this->getRepository();
        $arrayUsers = $repository->findByUsername($userName);

        return current($arrayUsers);
    }

    public function add(Domain\User $user){
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    /**
     * Remove an User from Repository
     * @param User $user
     * @return bool
     */
    public function remove(Domain\User $user)
    {
        $this->getEntityManager()->remove($user);
        $this->getEntityManager()->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityPath()
    {
        return $this->entityPath;
    }
}
