<?php

namespace AppBundle\Domain;

interface UrlRepository extends Repository
{

    /**
     * @param Url $url
     * @return Url
     */
    public function add(Url $url);

    /**
     * @param Url $url
     * @return bool
     */
    public function remove(Url $url);

    /**
     * @param $realUrl
     * @return Url
     */
    public function getByRealUrl($realUrl);
}
