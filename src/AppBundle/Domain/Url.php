<?php

namespace AppBundle\Domain;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="url")
 * @ORM\Entity(repositoryClass="AppBundle\Infrastructure\UrlRepository")
 */
class Url
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $shortUrl;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $realUrl;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="urls")
     * @ORM\JoinColumn(name="fk_user", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $hits = 0;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getShortUrl()
    {
        return $this->shortUrl;
    }

    /**
     * @param string $shortUrl
     */
    public function setShortUrl($shortUrl)
    {
        $this->shortUrl = $shortUrl;
    }

    /**
     * @return mixed
     */
    public function getRealUrl()
    {
        return $this->realUrl;
    }

    /**
     * @param mixed $realUrl
     */
    public function setRealUrl($realUrl)
    {
        $this->realUrl = $realUrl;
        if (!$this->shortUrl) {
            $this->generateShortUrl();
        }
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param int $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getHits()
    {
        return $this->hits;
    }

    /**
     * @param int $hits
     */
    public function setHits($hits)
    {
        $this->hits = $hits;
    }

    private function generateShortUrl()
    {
        $shortUrl = uniqid("cha");
        $this->setShortUrl($shortUrl);
    }

    public function addHit()
    {
        $this->hits += 1;
    }

    public function toArray($baseUrl = null)
    {
        if ($baseUrl) {
            $short = $this->getShortUrl();
            $this->setShortUrl($baseUrl. $short);
        }
        
        $urlArray = get_object_vars($this);
        
         
        unset($urlArray['createdAt']);
        unset($urlArray['updatedAt']);
        unset($urlArray['user']);
        return $urlArray;
    }

}
