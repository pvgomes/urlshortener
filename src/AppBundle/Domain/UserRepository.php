<?php

namespace AppBundle\Domain;

interface UserRepository extends Repository
{
    /**
     * Add an User on Repository
     * @param User $user
     * @return User
     */
    public function add(User $user);

    /**
     * Remove an User from Repository
     * @param User $user
     * @return bool
     */
    public function remove(User $user);
}
